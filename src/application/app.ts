import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import path from "path";
import exceptionsFilter from "./middlewares/exceptions-filter.middleware";
import logger from "./middlewares/logger.middleware";
import auth from "./middlewares/authorization.middleware";
import { AppDataSource } from "../configs/database/data-source";
import postsRouter from "../posts/posts.router";

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(logger);
// app. use(auth);

AppDataSource.initialize()
  .then(() => {
    console.log(" Typeorm connected to database");
  })
  .catch((error) => {
    console.log("Error: ", error);
  });

const staticFilesPath = path.join(__dirname, "../", "public");

app.use("/api/v1/public", express.static(staticFilesPath));
app.use("/api/v1/posts", postsRouter);

app.use(exceptionsFilter);

export default app;
