import Joi from "joi";

export const idParamSchema = Joi.object<{ id: string }>({
  id: Joi.string().uuid().length(36).required(),
});
