export interface ICore {
  id: string;
  createdAt: string;
  updatedAt: string;
}
