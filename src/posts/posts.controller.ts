import { Request, Response } from "express";
import { ValidatedRequest } from "express-joi-validation";
import * as postsService from "./posts.service";
import { IPostCreateRequest } from "./types/post-create-request.interface";
import { IPostUpdateRequest } from "./types/post-update-request.interface";

export const getAllPosts = async (request: Request, response: Response) => {
  const posts = await postsService.getAllPosts();
  response.json(posts);
};

export const getPostById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const post = await postsService.getPostById(id);
  response.json(post);
};

export const createPost = async (
  request: ValidatedRequest<IPostCreateRequest>,
  response: Response
) => {
  const post = await postsService.createPost(request.body);
  response.status(201).json(post);
};

export const updatePostById = async (
  request: ValidatedRequest<IPostUpdateRequest>,
  response: Response
) => {
  const { id } = request.params;
  const result = await postsService.updatePostById(id, request.body);
  response.status(204).send();
};

export const deletePostById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const result = await postsService.deletePostById(id);
  response.status(204).send();
};
