import express from "express";
import validator from "../application/middlewares/validation.middleware";
import controllerWrapper from "../application/utilities/controller-wrapper";
import { postCreateSchema, postUpdateSchema } from "./posts.schema";
import * as postsController from "./posts.controller";
import { idParamSchema } from "../application/schemas/id-param.schema";

const router = express.Router();

router.get("/", controllerWrapper(postsController.getAllPosts));

router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(postsController.getPostById)
);

router.post(
  "/",
  validator.body(postCreateSchema),
  controllerWrapper(postsController.createPost)
);

router.patch(
  "/:id",
  validator.params(idParamSchema),
  validator.body(postUpdateSchema),
  controllerWrapper(postsController.updatePostById)
);

router.delete(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(postsController.deletePostById)
);

export default router;
