import Joi from "joi";
import { IPost } from "./types/post.interface";
import { ICore } from "../application/types/core.interface";

export const postCreateSchema = Joi.object<Omit<IPost, keyof ICore>>({
  title: Joi.string().required(),
  description: Joi.string().required(),
});

export const postUpdateSchema = Joi.object<Omit<IPost, keyof ICore>>({
  title: Joi.string().optional(),
  description: Joi.string().optional(),
});
