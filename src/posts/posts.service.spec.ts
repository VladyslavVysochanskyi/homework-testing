import { DeleteResult, UpdateResult } from "typeorm";
import { HttpStatuses } from "../application/enums/http-statuses.enum";
import HttpException from "../application/exceptions/http-exception";
import { AppDataSource } from "../configs/database/data-source";
import { Post } from "./entities/post.entity";
import {
  getAllPosts,
  getPostById,
  createPost,
  updatePostById,
  deletePostById,
} from "./posts.service";

const postsRepository = AppDataSource.getRepository(Post);

afterEach(() => {
  jest.clearAllMocks();
});

describe("#createPost", () => {
  it("calls postsRepository.save with correct params", async () => {
    const spy = jest
      .spyOn(postsRepository, "save")
      .mockImplementation((() => Promise.resolve()) as any);

    const title = "title";
    const description = "description";
    await createPost({ title, description });

    expect(spy).toHaveBeenCalledWith({ title, description });
  });

  it("returns result of postsRepository.save", async () => {
    const title = "title";
    const description = "description";
    const expectedResult = { title, description };

    const spy = jest
      .spyOn(postsRepository, "save")
      .mockImplementation((() => Promise.resolve(expectedResult)) as any);

    const result = await createPost({ title, description });

    expect(result).toEqual(expectedResult);
  });
});

describe("#getAllPosts", () => {
  it("calls postsRepository.find with correct params", async () => {
    const spy = jest
      .spyOn(postsRepository, "find")
      .mockImplementation((() => Promise.resolve()) as any);

    await getAllPosts();

    expect(spy).toHaveBeenCalledWith({
      order: {
        createdAt: "DESC",
      },
    });
  });

  it("returns an empty result of postsRepository.save", async () => {
    const spy = jest
      .spyOn(postsRepository, "find")
      .mockImplementation((() => Promise.resolve([])) as any);

    const result = await getAllPosts();

    expect(result).toEqual([]);
    expect(result.length).toBe(0);
  });

  it("returns a non-empty result of postsRepository.save", async () => {
    const expectedPosts = [
      {
        id: "550e8400-e29b-41d4-a716-446655440000",
        title: "Test Post1",
        description: "Test Post",
        createdAt: "2023-05-12T12:34:45Z",
        updatedAt: "2023-05-12T12:34:45Z",
      },
      {
        id: "550e8400-e29b-41d4-a716-446655440000",
        title: "Test Post1",
        description: "Test Post",
        createdAt: "2023-05-12T12:34:45Z",
        updatedAt: "2023-05-12T12:34:45Z",
      },
    ];

    const spy = jest
      .spyOn(postsRepository, "find")
      .mockImplementation((() => Promise.resolve(expectedPosts)) as any);

    const result = await getAllPosts();

    expect(result).toEqual(expectedPosts);
    expect(result.length).toBeGreaterThan(0);
  });
});

describe("#getPostById", () => {
  it("calls postsRepository.findOne with correct params", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";
    const mockPost = {
      id,
      title: "Test Post",
      description: "Test Post",
      createdAt: "2023-05-12T12:34:45Z",
      updatedAt: "2023-05-12T12:34:45Z",
    };
    const spy = jest
      .spyOn(postsRepository, "findOne")
      .mockImplementation((() => Promise.resolve(mockPost)) as any);

    const result = await getPostById(id);

    expect(spy).toHaveBeenCalledWith({
      where: {
        id,
      },
    });
  });

  it("should throw an error if post with the given id is not found", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";

    const spy = jest
      .spyOn(postsRepository, "findOne")
      .mockImplementation((() => Promise.resolve()) as any);

    await expect(getPostById(id)).rejects.toThrow(
      new HttpException(HttpStatuses.NOT_FOUND, "Post with this id not found")
    );
  });

  it("should return a post by id", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";
    const mockPost = {
      id,
      title: "Test Post",
      description: "Test Post",
      createdAt: "2023-05-12T12:34:45Z",
      updatedAt: "2023-05-12T12:34:45Z",
    };
    const spy = jest
      .spyOn(postsRepository, "findOne")
      .mockImplementation((() => Promise.resolve(mockPost)) as any);

    const result = await getPostById(id);

    expect(result).toEqual(mockPost);
  });
});

describe("#updatePostById", () => {
  it("calls postsRepository.update with correct params", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";
    const mockPost = {
      id,
      title: "Test Post",
      description: "Test Post",
      createdAt: "2023-05-12T12:34:45Z",
      updatedAt: "2023-05-12T12:34:45Z",
    };
    const spy = jest
      .spyOn(postsRepository, "update")
      .mockImplementation((() => Promise.resolve({ affected: 1 })) as any);

    const result = await updatePostById(id, mockPost);

    expect(spy).toHaveBeenCalledWith(id, mockPost);
  });

  it("should update a post by id", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";
    const mockUpdateData = {
      title: "Test Post",
      description: "Test Post",
    };
    const expectedResult = { affected: 1 } as UpdateResult;

    const spy = jest
      .spyOn(postsRepository, "update")
      .mockImplementation((() => Promise.resolve(expectedResult)) as any);

    const result = await updatePostById(id, mockUpdateData);

    expect(result).toEqual(expectedResult);
  });

  it("should throw an error if post with the given id is not found", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";
    const mockUpdateData = {
      title: "Test Post",
      description: "Test Post",
    };
    const spy = jest
      .spyOn(postsRepository, "update")
      .mockImplementation((() => Promise.resolve({ affected: 0 })) as any);

    await expect(updatePostById(id, mockUpdateData)).rejects.toThrow(
      new HttpException(HttpStatuses.NOT_FOUND, "Post with this id not found")
    );
  });
});

describe("#deletePostById", () => {
  it("calls postsRepository.delete with correct params", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";

    const spy = jest
      .spyOn(postsRepository, "delete")
      .mockImplementation((() => Promise.resolve({ affected: 1 })) as any);

    const result = await deletePostById(id);

    expect(spy).toHaveBeenCalledWith(id);
  });

  it("should delete a post by id", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";
    const expectedResult = { affected: 1 } as DeleteResult;

    const spy = jest
      .spyOn(postsRepository, "delete")
      .mockImplementation((() => Promise.resolve(expectedResult)) as any);

    const result = await deletePostById(id);

    expect(result).toEqual(expectedResult);
  });

  it("should throw an error if post with the given id is not found", async () => {
    const id = "550e8400-e29b-41d4-a716-446655440000";

    const spy = jest
      .spyOn(postsRepository, "delete")
      .mockImplementation((() => Promise.resolve({ affected: 0 })) as any);

    await expect(deletePostById(id)).rejects.toThrow(
      new HttpException(HttpStatuses.NOT_FOUND, "Post with this id not found")
    );
  });
});
