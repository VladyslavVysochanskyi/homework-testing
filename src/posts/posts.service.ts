import { IPost } from "./types/post.interface";
import { AppDataSource } from "../configs/database/data-source";
import { Post } from "./entities/post.entity";
import { ICore } from "../application/types/core.interface";
import { UpdateResult, DeleteResult } from "typeorm";
import { HttpStatuses } from "../application/enums/http-statuses.enum";
import HttpException from "../application/exceptions/http-exception";

const postsRepository = AppDataSource.getRepository(Post);

export const getAllPosts = async (): Promise<Post[]> => {
  return postsRepository.find({
    order: {
      createdAt: "DESC",
    },
  });
};

export const getPostById = async (id: string): Promise<Post> => {
  const post = await postsRepository.findOne({
    where: {
      id,
    },
  });

  if (!post) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Post with this id not found"
    );
  }
  return post;
};

export const createPost = async (
  createStudentSchema: Omit<IPost, keyof ICore>
): Promise<Post> => {
  return postsRepository.save(createStudentSchema);
};

export const updatePostById = async (
  id: string,
  updateStudentSchema: Partial<Omit<IPost, keyof ICore>>
): Promise<UpdateResult> => {
  const result = await postsRepository.update(id, updateStudentSchema);
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Post with this id not found"
    );
  }
  return result;
};

export const deletePostById = async (id: string): Promise<DeleteResult> => {
  const result = await postsRepository.delete(id);
  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Post with this id not found"
    );
  }
  return result;
};
