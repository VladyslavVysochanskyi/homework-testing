import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { IPost } from "./post.interface";
import { ICore } from "../../application/types/core.interface";

export interface IPostCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IPost, keyof ICore>;
}
