import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { IPost } from "./post.interface";
import { ICore } from "../../application/types/core.interface";

export interface IPostUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Omit<IPost, keyof ICore>>;
}
