import { ICore } from "../../application/types/core.interface";

export interface IPost extends ICore {
  title: string;
  description: string;
}
