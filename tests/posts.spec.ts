import "dotenv/config";
import { AppDataSource } from "../src/configs/database/data-source";
import { Post } from "../src/posts/entities/post.entity";
import request from "supertest";
import app from "../src/application/app";
import { DataSource } from "typeorm";

let dataSource: DataSource;

const postRepository = AppDataSource.getRepository(Post);

beforeAll(async () => {
  dataSource = await AppDataSource.initialize();
});

beforeEach(async () => {
  await postRepository.clear();
});

afterAll(async () => {
  await dataSource.destroy();
});

describe("/posts", () => {
  describe("POST /posts", () => {
    it("returns 400 status if title is not passed", async () => {
      await request(app)
        .post("/api/v1/posts")
        .send({ description: "Hello" })
        .expect(400);
    });

    it("returns 400 status if description is not passed", async () => {
      await request(app)
        .post("/api/v1/posts")
        .send({ title: "Hello" })
        .expect(400);
    });

    it("saves post", async () => {
      const title = "New Post";
      const description = "description Post with info";
      console.log(123132);
      await request(app)
        .post("/api/v1/posts")
        .send({ title, description })
        .expect(201);

      const posts = await postRepository.find();
      expect(posts.length).toEqual(1);
      expect(posts[0]).toMatchObject({ title, description });
    });

    it("returns created post", async () => {
      const response = await request(app)
        .post("/api/v1/posts")
        .send({ title: "New Post", description: "description Post with info" })
        .expect(201);

      const posts = await postRepository.find();
      expect(posts.length).toEqual(1);
      const { id, title, description, createdAt, updatedAt } = posts[0];
      expect(response.body).toEqual({
        id,
        title,
        description,
        createdAt: createdAt.toISOString(),
        updatedAt: updatedAt.toISOString(),
      });
    });
  });

  describe("GET /posts", () => {
    it("returns an empty list if no posts", async () => {
      const response = await request(app).get("/api/v1/posts").expect(200);
      expect(response.body).toEqual([]);
    });

    it("returns all saved posts in sorted (DESC) order by createdAt", async () => {
      const post1 = { title: "Post 1", description: "Description 1" };
      const post2 = { title: "Post 2", description: "Description 2" };
      const post3 = { title: "Post 3", description: "Description 3" };

      const createdPost1 = await request(app)
        .post("/api/v1/posts")
        .send(post1)
        .expect(201);
      await new Promise((resolve) => setTimeout(resolve, 1000));

      const createdPost2 = await request(app)
        .post("/api/v1/posts")
        .send(post2)
        .expect(201);
      await new Promise((resolve) => setTimeout(resolve, 1000));

      const createdPost3 = await request(app)
        .post("/api/v1/posts")
        .send(post3)
        .expect(201);

      const response = await request(app).get("/api/v1/posts").expect(200);
      const sortedPosts = [
        createdPost1.body,
        createdPost2.body,
        createdPost3.body,
      ].sort(
        (a, b) =>
          new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
      );

      expect(response.body).toEqual(sortedPosts);
    });
  });

  describe("GET /posts/:id", () => {
    it("returns 400 status if bad request", async () => {
      await request(app).get("/api/v1/posts/1").expect(400);
    });

    it("returns 404 status if post not found", async () => {
      await request(app)
        .get("/api/v1/posts/550e8400-e29b-41d4-a716-446655440000")
        .expect(404);
    });

    it("returns the specified post", async () => {
      const { body: createdPost } = await request(app)
        .post("/api/v1/posts")
        .send({
          title: "New Post",
          description: "description Post with info",
        })
        .expect(201);

      const response = await request(app)
        .get(`/api/v1/posts/${createdPost.id}`)
        .expect(200);
      expect(response.body).toEqual(createdPost);
    });
  });

  describe("PATCH /posts/:id", () => {
    it("returns 400 status if bad request", async () => {
      await request(app)
        .patch("/api/v1/posts/1")
        .send({ title: "Updated Post" })
        .expect(400);
    });

    it("returns 404 status if post not found", async () => {
      await request(app)
        .patch("/api/v1/posts/550e8400-e29b-41d4-a716-446655440000")
        .send({ title: "Updated Post" })
        .expect(404);
    });

    it("updates the title of specified post", async () => {
      const { body: createdPost } = await request(app)
        .post("/api/v1/posts")
        .send({
          title: "New Post",
          description: "description Post with info",
        })
        .expect(201);

      const updatedTitle = "Updated Post";
      await request(app)
        .patch(`/api/v1/posts/${createdPost.id}`)
        .send({ title: updatedTitle })
        .expect(204);

      const updatedPost = await postRepository.findOne({
        where: {
          id: createdPost.id,
        },
      });
      expect(updatedPost?.title).toEqual(updatedTitle);
    });

    it("updates the description of specified post", async () => {
      const { body: createdPost } = await request(app)
        .post("/api/v1/posts")
        .send({
          title: "New Post",
          description: "description Post with info",
        })
        .expect(201);

      const updatedDescription = "description";
      await request(app)
        .patch(`/api/v1/posts/${createdPost.id}`)
        .send({ description: updatedDescription })
        .expect(204);

      const updatedPost = await postRepository.findOne({
        where: {
          id: createdPost.id,
        },
      });
      expect(updatedPost?.description).toEqual(updatedDescription);
    });

    it("updates specified post fully", async () => {
      const { body: createdPost } = await request(app)
        .post("/api/v1/posts")
        .send({
          title: "New Post",
          description: "description Post with info",
        })
        .expect(201);

      const updatedTitle = "Updated Post";
      const updatedDescription = "description";
      await request(app)
        .patch(`/api/v1/posts/${createdPost.id}`)
        .send({ title: updatedTitle, description: updatedDescription })
        .expect(204);

      const updatedPost = await postRepository.findOne({
        where: {
          id: createdPost.id,
        },
      });
      expect(updatedPost?.title).toEqual(updatedTitle);
      expect(updatedPost?.description).toEqual(updatedDescription);
    });
  });

  describe("DELETE /posts/:id", () => {
    it("returns 400 status if bad request", async () => {
      await request(app).get("/api/v1/posts/1").expect(400);
    });
    it("returns 404 status if post not found", async () => {
      await request(app)
        .delete("/api/v1/posts/550e8400-e29b-41d4-a716-446655440000")
        .expect(404);
    });
    it("deletes the specified post", async () => {
      const { body: createdPost } = await request(app)
        .post("/api/v1/posts")
        .send({
          title: "New Post",
          description: "description Post with info",
        })
        .expect(201);

      await request(app).delete(`/api/v1/posts/${createdPost.id}`).expect(204);

      const deletedPost = await postRepository.findOne({
        where: {
          id: createdPost.id,
        },
      });
      expect(deletedPost).toBeNull();
    });
  });
});
